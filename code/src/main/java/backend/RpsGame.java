//Ahmed Sobh 
//Student ID: 2134222

package backend;

import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    Random rand;

    public RpsGame(){
        wins = 0;
        ties = 0;
        losses = 0;
        rand = new Random();
    }

    public int getWins() {
        return wins;
    }

    public int getTies() {
        return ties;
    }

    public int getLosses() {
        return losses;
    }

    public String playRound(String pChoice){
        int upperBound = 3;
        int cChoice = rand.nextInt(upperBound);
        switch(cChoice){
            case 0: 
                if(pChoice.equals("ROCK")){
                    ties ++;
                    return "The computer plays rock its a draw!";
                }
                else if(pChoice.equals("PAPER")){
                    wins ++;
                    return "The computer plays rock the player wins!";

                }
                else{
                    losses ++;
                    return "The computer plays rock the computer wins!";
                }
            case 1:
                if(pChoice.equals("ROCK")){
                    wins ++;
                    return "The computer plays scissors the player wins!";
                }
                else if(pChoice.equals("PAPER")){
                    losses ++;
                    return "The computer plays scissors the computer wins!";
                }
                else{
                    ties ++;
                    return "The computer plays scissors its a draw!";
                }
            case 2:
                if(pChoice.equals("ROCK")){
                    losses ++;
                    return "The computer plays paper the computer wins!";
                }
                else if(pChoice.equals("PAPER")){
                    ties ++;
                    return "The computer plays paper its a draw!";
                }
                else{
                    wins ++;
                    return "The computer plays paper the player wins!";
                }
            default:
               return "Something went wrong";
        }
    }
}

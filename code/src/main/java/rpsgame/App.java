//Ahmed Sobh 
//Student ID: 2134222

package rpsgame;

import java.util.Scanner;

import backend.RpsGame;

/**
 * Console application for rock paper scissors game
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        RpsGame game = new RpsGame();
        boolean quit = false;
        Scanner scan = new Scanner(System.in);
        String pChoice;
        while(!quit){
            System.out.println("Please enter rock, paper or scissors");
            pChoice = scan.next();
            if(pChoice.toUpperCase().equals("QUIT")){
                quit = true;
                System.out.println("Current session ended with these scores:\nWins: " + game.getWins() + "\tLosses: " + game.getLosses() + "\tTies: " + game.getTies());
            }
            else{
                System.out.println(game.playRound(pChoice.toUpperCase()));
                System.out.println("Current session:\nWins: " + game.getWins() + "\tLosses: " + game.getLosses() + "\tTies: " + game.getTies());
            }
        }
    }
}
